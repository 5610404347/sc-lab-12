package no1;

import java.util.ArrayList;

public class PhoneBook {
	public ArrayList<String> name, phoneNumber;  
	
	public PhoneBook() {
		this.name = new ArrayList<String>();
		this.phoneNumber = new ArrayList<String>();
	}
	
	public void add(String line){
		String[] item = line.split(", ");
		name.add(item[0]);
		phoneNumber.add(item[1]);
	}

	public String toString(){
		String result = "--------- Java Phone Book ---------\nName Phone\n==== =====\n";
		for (int i = 0; i < name.size(); i++) {
			result += (name.get(i) + " " + phoneNumber.get(i) + "\n");
		}
		return result;
	}
}
