package no1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;



public class PhoneBookMain {

	public static void main(String[] args) throws IOException {
		
		String filename = "phonebook.txt";

		try {
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			
			PhoneBook p = new PhoneBook();
			

			for (String line = buffer.readLine(); line != null; line = buffer.readLine()){
				p.add(line);
			}
			System.out.println(p.toString());
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
			}
		catch (IOException e){
			System.err.println("Error reading from file");
			}
	}
	
	

}
