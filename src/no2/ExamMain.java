package no2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class ExamMain {

	public static void main(String[] args) {
		String filename = "exam.txt";

		try {
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			
			Exam e = new Exam();
			
			for (String line = buffer.readLine(); line != null; line = buffer.readLine()){
				e.add(line);
			}
			
			System.out.println(e.toString());
			
			FileWriter fileWriter = null;
			fileWriter = new FileWriter("average.txt");
			BufferedWriter out = new BufferedWriter(fileWriter);
			out.write(e.toString());
			out.newLine();
			out.flush();
			
			
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
			}
		catch (IOException e){
			System.err.println("Error reading from file");
			}
	}
}

