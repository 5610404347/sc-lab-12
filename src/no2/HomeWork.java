package no2;

import java.util.ArrayList;

public class HomeWork {
	public ArrayList<String> name; 
	public ArrayList<Double> avg; 
	
	public HomeWork(){
		this.name = new ArrayList<String>();
		this.avg = new ArrayList<Double>();
	}
	
	public void add(String line){
		String[] item = line.split(", ");
		name.add(item[0]);
		
		double average = 0;
		int i;
		for (i = 1; i < item.length; i++) {
			average += Double.parseDouble(item[i]);
		}
		
		average = average / (i-1);
		
		avg.add(average);
	}
	
	public String toString(){
		String result = "--------- Homework Scores ---------\nName Average\n==== =======\n";
		for (int i = 0; i < name.size(); i++) {
			result += (name.get(i) + " " + avg.get(i) + "\n");
		}
		return result;
	}
}
